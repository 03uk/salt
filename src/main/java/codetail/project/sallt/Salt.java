package codetail.project.sallt;


import android.content.ComponentName;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * LocalBroadcastManager with enchantments
 *
 * instead of sending {@link android.content.Intent} to the registered components,
 * we are going to send them to the {@link Presenter} and apply Models that it keeps
 */
public class Salt {

    private static final String TAG = "Salt";
    private static boolean DEBUG = false;

    public static void setDebugging(boolean debugging){
        DEBUG = debugging;
    }

    private static final int MSG_EXEC_PENDING_BROADCASTS = 1;

    private static class BroadcastRecord {
        Presenter presenter;
        Object target;

        public BroadcastRecord(Presenter presenter, Object target) {
            this.presenter = presenter;
            this.target = target;
        }

        @Override
        public String toString() {
            return "BroadcastRecord{" + classNameOf(target) +
                    " presenter=" + presenter +
                    "}";
        }
    }

    private final Handler mHandler;

    private final HashMap<String, Object> mReceivers = new HashMap<>();
    private final ArrayList<BroadcastRecord> mPendingBroadcasts = new ArrayList<>();

    private static final Object LOCK = new Object();
    private static Salt INSTANCE;

    public static Salt getInstance(){
        synchronized (LOCK){
            if(INSTANCE == null) {
                INSTANCE = new Salt();
            }

            return INSTANCE;
        }
    }

    public Salt() {
        mHandler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what){
                    case MSG_EXEC_PENDING_BROADCASTS:
                        executePendingBroadcasts();
                        break;

                    default: super.handleMessage(msg);
                }
            }
        };
    }

    public boolean sendBroadcast(ComponentName component, Presenter presenter){
        synchronized (mReceivers){
            Object target = mReceivers.get(component.getClassName());
            if(target == null){
                if(DEBUG){
                    android.util.Log.d(TAG, "No recievers found for " +
                            component.flattenToString());
                }

                return false;
            }

            mPendingBroadcasts.add(new BroadcastRecord(presenter, target));
            if(!mHandler.hasMessages(MSG_EXEC_PENDING_BROADCASTS)){
                mHandler.sendEmptyMessage(MSG_EXEC_PENDING_BROADCASTS);
            }

            return true;
        }
    }

    public void sendBroadcastSync(ComponentName componentName, Presenter presenter){
        if(sendBroadcast(componentName, presenter)){
            executePendingBroadcasts();
        }
    }

    /**
     * Register object, it will be passed to {@link Presenter#apply(Object)}
     *
     * @see Presenter#apply(Object)
     * @see BackgroundTask#BackgroundTask(ComponentName)
     */
    public void register(Object object){
        synchronized (mReceivers){
            mReceivers.put(classNameOf(object), object);
        }
    }

    /**
     * Unregister object
     */
    public void unregister(Object object){
        synchronized (mReceivers){
            mReceivers.remove(classNameOf(object));
        }
    }

    @SuppressWarnings("unchecked")
    private void executePendingBroadcasts(){
        while (true){
            BroadcastRecord[] records = null;
            synchronized (mReceivers){
                final int N = mPendingBroadcasts.size();
                if (N <= 0) {
                    return;
                }
                records = new BroadcastRecord[N];
                mPendingBroadcasts.toArray(records);
                mPendingBroadcasts.clear();
            }

            for (BroadcastRecord record : records) {
                record.presenter.apply(record.target);
            }
        }
    }

    static String classNameOf(Object target){
        return target.getClass().getName();
    }
}
