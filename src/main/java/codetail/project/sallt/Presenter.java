package codetail.project.sallt;

import android.content.ComponentName;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Presenter is the class that will apply Models on View
 *
 * Logic is the next, do big work in {@link BackgroundTask} and return how to
 * present your Models via {@link Presenter}, in additional keep your models
 * here, and apply them on the main thread
 *
 * @param <View> Registered component in {@link BackgroundTask} && {@link Salt#register(Object)}
 *
 * @see Salt#register(Object)
 * @see BackgroundTask#BackgroundTask(ComponentName)
 */
public interface Presenter<View> extends Parcelable {

    /**
     * Apply method called immediately after {@link BackgroundTask#doInBackground(Object[])}
     * (!note, if task wasn't cancelled), on the main thread
     *
     * @param view View on whom needs to apply
     */
    void apply(View view);

    /**
     * Empty presenter
     */
    class Empty<View> implements Presenter<View> {

        @Override
        public void apply(View view) {

        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {

        }
    }
}
